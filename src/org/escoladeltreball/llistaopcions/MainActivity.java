package org.escoladeltreball.llistaopcions;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class MainActivity extends ListActivity implements OnItemClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		String[] values = new String[] { "Obrir pàgina web", "Obrir contactes",
				"Obrir telèfon", "Cerca Google" };

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, values);

		setListAdapter(adapter);

		getListView().setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		switch (position) {
		case 0:
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://www.android.com")));
			break;
		case 1:
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("content://contacts/people/")));
			break;
		case 2:
			startActivity(new Intent(Intent.ACTION_DIAL));
			break;
		case 3:
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://www.google.com")));
			break;
		}
	}

}
